/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package version2;

import com.sun.xml.internal.ws.util.StringUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author uttam
 */
public class Version2 {

    /**
     * @param args the command line arguments
     */
    static int p = 997;
    static int q = 997;
    static int g = 7;
    static int CompletePassword = 0;
    static int passwordA=0;
    static int passwordB=0;
    static int password1=0;
    static int password2=0;
    static BigInteger A,DA,NA,PHIA;
    static BigInteger B,DB,NB;
    static PrivateKey Ska,Skb,Dc;
    static PublicKey Pka,Pkb,C;
    static int rc,wc,ra,wa,rb,wb;
    static BigInteger Ra,Wa,Rb,Wb,Rc,Wc,Skca,Skcb,omegaA,omegaB;
    String HA,HAD,HB,HBD,H1,H2,EA,EB,H1d,H2d,EDA,EDB;
    byte[] bEA,bEB,bEDA,bEDB;
    public void getPassword(){
		System.out.println("Enter the complete password(all should be integers[less than 997 as q is 997])");
		
		Scanner sc = new Scanner(System.in);
		CompletePassword = sc.nextInt();

		//generate pw-a n pw-b
		int maxv = CompletePassword - 1;
		int minv = 1;
                
                Random randomNum = new Random();
                passwordA = minv + randomNum.nextInt(maxv);
//                passwordB = CompletePassword - passwordA ;
                int i;
                for(i=0;i<CompletePassword;i++){
                    System.out.println((passwordA+i)%q);
                    if((passwordA + i )%q == CompletePassword){
                        passwordB = i;
                        break;
                    }
                }
                System.out.println("Complete password is :"+CompletePassword);
                System.out.println("passwordA is: " + passwordA);
                System.out.println("passwordB is:" + passwordB);
	}
    public void generatePassword1AndPassword2(){
                int maxv = CompletePassword - 1;
		int minv = 1;
                
                Random randomNum = new Random();
                password1 = minv + randomNum.nextInt(maxv);
//                passwordB = CompletePassword - passwordA ;
                int i;
                for(i=0;i<CompletePassword;i++){
//                    System.out.println((passwordA+i)%q);
                    if((password1 + i )%q == CompletePassword){
                        password2 = i;
                        break;
                    }
                }
                System.out.println("Complete password is :"+CompletePassword);
                System.out.println("password1 is: " + password1);
                System.out.println("password2 is:" + password2);
    }
    public void generateBandDB(int pwb,int g){
        BigInteger one = new BigInteger("1");
        BigInteger p = one.probablePrime(1024,new Random());
        BigInteger q = one.probablePrime(1024,new Random());
        BigInteger n = p.multiply(q);       
        BigInteger phi = p.subtract(one).multiply(q.subtract(one));
        BigInteger Bigg =  BigInteger.valueOf(g);
//        BigInteger Bigpwa = BigInteger.valueOf(pwa);
        BigInteger e = Bigg.pow(pwb);
        while(e.compareTo(phi)<0)
       {
           if(phi.gcd(e).equals(one))
               break;
           e=e.add(one);
       }      
       BigInteger d = e.modInverse(phi); 
       System.out.println("Enter your message (plaintext) : ");
       Scanner s = new Scanner(System.in);
       BigInteger plaintext = s.nextBigInteger();
       BigInteger cipher = plaintext.modPow(e,n);
       System.out.println(" Ciphertext is : "+cipher);      
       System.out.println(" Upon decryptin we get back plaintext : "+cipher.modPow(d,n));
       System.out.println("Encryption key is :"+e);
       System.out.println("Decryption key is :"+d);
       B = e;
       DB = d;
       NB = n;
    }
    
    public void generateAandDA(int pwa,int g){
        BigInteger one = new BigInteger("1");
        BigInteger p = one.probablePrime(1024,new Random());
        BigInteger q = one.probablePrime(1024,new Random());
        BigInteger n = p.multiply(q);       
        BigInteger phi = p.subtract(one).multiply(q.subtract(one));
        BigInteger Bigg =  BigInteger.valueOf(g);
//        BigInteger Bigpwa = BigInteger.valueOf(pwa);
        BigInteger e = Bigg.pow(pwa);
        while(e.compareTo(phi)<0)
       {
           if(phi.gcd(e).equals(one))
               break;
           e=e.add(one);
       }      
       BigInteger d = e.modInverse(phi); 
       System.out.println("Enter your message (plaintext) : ");
       Scanner s = new Scanner(System.in);
       BigInteger plaintext = s.nextBigInteger();
       BigInteger cipher = plaintext.modPow(e,n);
       System.out.println(" Ciphertext is : "+cipher);      
       System.out.println(" Upon decryptin we get back plaintext : "+cipher.modPow(d,n));
       System.out.println("Encryption key is :"+e);
       System.out.println("Decryption key is :"+d);
       A = e;
       DA = d;
       NA = n;
       PHIA = phi;
    }
    
    public void generateCAndDc(int keylength) throws NoSuchAlgorithmException{
        KeyPairGenerator keyGen =  KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(keylength);
        KeyPair pair = keyGen.generateKeyPair();
        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();
        C = publicKey;
        Dc = privateKey;
        
    }
    public void generatePkaAndSka(int keylength) throws NoSuchAlgorithmException{
        KeyPairGenerator keyGen =  KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(keylength);
        KeyPair pair = keyGen.generateKeyPair();
        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();
        Pka = publicKey;
        Ska = privateKey;
        
        
    }
    public void generatePkbAndSkb(int keylength) throws NoSuchAlgorithmException{
        KeyPairGenerator keyGen =  KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(keylength);
        KeyPair pair = keyGen.generateKeyPair();
        PrivateKey privateKey = pair.getPrivate();
        PublicKey publicKey = pair.getPublic();
        Pkb = publicKey;
        Skb = privateKey;
        
        
    }
   
    public void generateRaAndWa(){
        BigInteger p=new BigInteger("997");
        
        BigInteger g=new BigInteger("7");
        System.out.println("Enter Ra value  less than "+p+":");
        Scanner sc = new Scanner(System.in);
//        BigInteger Ra;
        Ra = new BigInteger(sc.next());
        Wa=g.modPow(Ra,p);
        
        
    }
    public void generateRbAndWb(){
        BigInteger p=new BigInteger("997");
        
        BigInteger g=new BigInteger("7");
        System.out.println("Enter Rb value  less than "+p+":");
        Scanner sc = new Scanner(System.in);
//        BigInteger Ra;
        Rb = new BigInteger(sc.next());
        Wb =g.modPow(Rb,p);
        
        
    }
    
    
    public void generateRcAndWc(){
        BigInteger p=new BigInteger("997");
        
        BigInteger g=new BigInteger("7");
        System.out.println("Enter Rc value  less than "+p+":");
        Scanner sc = new Scanner(System.in);
//        BigInteger Ra;
        Rc = new BigInteger(sc.next());
        Wc=g.modPow(Rc,p);
        
        
    }
    public void writeStringToFile(String content,String Filename) throws FileNotFoundException{
        try(  PrintWriter out = new PrintWriter( Filename )  ){
           out.println( content );
        }
    }
    
    public String readFileToString(String filename) throws IOException,FileNotFoundException{
        FileReader fileReader = new FileReader(filename);
        String fileContents = "";
        int i ;
         
        while((i =  fileReader.read())!=-1){
            char ch = (char)i;
         
           fileContents = fileContents + ch; 
        }
         
          // System.out.println(fileContents);   
        return fileContents;
    }
    public void writeToFile(String path, byte[] key) throws IOException {

        File f = new File(path);
        f.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(key);
        fos.flush();
        fos.close();

    }
    public String GetHashOfFile(String filename) throws NoSuchAlgorithmException, IOException{

        MessageDigest sha1 = MessageDigest.getInstance("SHA1");
        FileInputStream fis = new FileInputStream(filename);
  
        byte[] data = new byte[1024];
        int read = 0; 
        while ((read = fis.read(data)) != -1) {
            sha1.update(data, 0, read);
        };
        byte[] hashBytes = sha1.digest();
  
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < hashBytes.length; i++) {
          sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
         
        String fileHash = sb.toString();
        return fileHash;
    }
    public void generateH1d() throws FileNotFoundException, NoSuchAlgorithmException, IOException{
        writeToFile("/home/uttam/Desktop/abhi/new/h1d/C.txt", C.getEncoded());
        writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/h1d/WC.txt");
        writeStringToFile(A.toString(10), "/home/uttam/Desktop/abhi/new/h1d/A.txt");
        writeStringToFile(Wa.toString(10), "/home/uttam/Desktop/abhi/new/h1d/WA.txt");
        
        String preH1d = GetHashOfFile("/home/uttam/Desktop/abhi/new/h1d/C.txt")
                    +GetHashOfFile("/home/uttam/Desktop/abhi/new/h1d/WC.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h1d/A.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h1d/WA.txt");
        writeStringToFile(preH1d,"/home/uttam/Desktop/abhi/new/h1d/preH1d.txt" );
        H1d = GetHashOfFile("/home/uttam/Desktop/abhi/new/h1d/preH1d.txt");
        writeStringToFile(H1d, "/home/uttam/Desktop/abhi/new/h1d/H1d.txt");
    }
    public void generateH1() throws IOException, NoSuchAlgorithmException{
        writeToFile("/home/uttam/Desktop/abhi/new/h1/C.txt", C.getEncoded());
        writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/h1/WC.txt");
        writeStringToFile(A.toString(10), "/home/uttam/Desktop/abhi/new/h1/A.txt");
        writeStringToFile(Wa.toString(10), "/home/uttam/Desktop/abhi/new/h1/WA.txt");
        
        String preH1 = GetHashOfFile("/home/uttam/Desktop/abhi/new/h1/C.txt")
                    +GetHashOfFile("/home/uttam/Desktop/abhi/new/h1/WC.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h1/A.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h1/WA.txt");
        writeStringToFile(preH1,"/home/uttam/Desktop/abhi/new/h1/preH1.txt" );
        H1 = GetHashOfFile("/home/uttam/Desktop/abhi/new/h1/preH1.txt");
        writeStringToFile(H1, "/home/uttam/Desktop/abhi/new/h1/H1.txt");
    }
    public void generateHa() throws IOException, NoSuchAlgorithmException{
//            a,wa,c,wc,pka
          writeToFile("/home/uttam/Desktop/abhi/new/ha/PKA.txt", Pka.getEncoded());
          writeToFile("/home/uttam/Desktop/abhi/new/ha/C.txt", C.getEncoded());
          writeStringToFile(A.toString(10), "/home/uttam/Desktop/abhi/new/ha/A.txt");
          writeStringToFile(Wa.toString(10), "/home/uttam/Desktop/abhi/new/ha/WA.txt");
          writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/ha/WC.txt");
          
          String preHA = GetHashOfFile("/home/uttam/Desktop/abhi/new/ha/A.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/ha/WA.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/ha/C.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/ha/WC.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/ha/PKA.txt");
          writeStringToFile(preHA,"/home/uttam/Desktop/abhi/new/ha/preHA.txt" );
          HA = GetHashOfFile("/home/uttam/Desktop/abhi/new/ha/preHA.txt");
          writeStringToFile(HA, "/home/uttam/Desktop/abhi/new/ha/HA.txt");
          
        
    }
    public void generateHad() throws FileNotFoundException, IOException, NoSuchAlgorithmException{
        writeToFile("/home/uttam/Desktop/abhi/new/had/PKA.txt", Pka.getEncoded());
        writeToFile("/home/uttam/Desktop/abhi/new/had/C.txt", C.getEncoded());
        writeStringToFile(A.toString(10), "/home/uttam/Desktop/abhi/new/had/A.txt");
        writeStringToFile(Wa.toString(10), "/home/uttam/Desktop/abhi/new/had/WA.txt");
        writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/had/WC.txt");
        
        String preHAD = GetHashOfFile("/home/uttam/Desktop/abhi/new/had/A.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/had/WA.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/had/C.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/had/WC.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/had/PKA.txt");
        writeStringToFile(preHAD,"/home/uttam/Desktop/abhi/new/had/preHAD.txt" );
        HAD = GetHashOfFile("/home/uttam/Desktop/abhi/new/had/preHAD.txt");
        writeStringToFile(HAD, "/home/uttam/Desktop/abhi/new/ha/HAD.txt");
        
    }
    public void generateH2d() throws FileNotFoundException, NoSuchAlgorithmException, IOException{
        writeToFile("/home/uttam/Desktop/abhi/new/h2d/C.txt", C.getEncoded());
        writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/h2d/WC.txt");
        writeStringToFile(B.toString(10), "/home/uttam/Desktop/abhi/new/h2d/B.txt");
        writeStringToFile(Wb.toString(10), "/home/uttam/Desktop/abhi/new/h2d/WB.txt");
        
        String preH2D = GetHashOfFile("/home/uttam/Desktop/abhi/new/h2d/C.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h2d/WC.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h2d/B.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h2d/WB.txt");
        writeStringToFile( preH2D,"/home/uttam/Desktop/abhi/new/h2d/preH2D.txt");      
        H2d = GetHashOfFile("/home/uttam/Desktop/abhi/new/h2d/preH2D.txt");
        writeStringToFile(H2d, "/home/uttam/Desktop/abhi/new/h2d/H2d.txt");
        
    }
    public void generateH2() throws IOException, NoSuchAlgorithmException{
        writeToFile("/home/uttam/Desktop/abhi/new/h2/C.txt", C.getEncoded());
        writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/h2/WC.txt");
        writeStringToFile(B.toString(10), "/home/uttam/Desktop/abhi/new/h2/B.txt");
        writeStringToFile(Wb.toString(10), "/home/uttam/Desktop/abhi/new/h2/WB.txt");
        
        String preH2 = GetHashOfFile("/home/uttam/Desktop/abhi/new/h2/C.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h2/WC.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h2/B.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/h2/WB.txt");
        writeStringToFile( preH2,"/home/uttam/Desktop/abhi/new/h2/preH2.txt");      
        H2 = GetHashOfFile("/home/uttam/Desktop/abhi/new/h2/preH2.txt");
        writeStringToFile(H2, "/home/uttam/Desktop/abhi/new/h2/H2.txt");
                  
                  
    }
    public void generateHb() throws IOException, NoSuchAlgorithmException{
        //        b,wb,c,wc,pkb
          writeToFile("/home/uttam/Desktop/abhi/new/hb/PKB.txt", Pkb.getEncoded());
          writeToFile("/home/uttam/Desktop/abhi/new/hb/C.txt", C.getEncoded());
          writeStringToFile(B.toString(10), "/home/uttam/Desktop/abhi/new/hb/B.txt");
          writeStringToFile(Wb.toString(10), "/home/uttam/Desktop/abhi/new/hb/WB.txt");
          writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/hb/WC.txt");
          
          String preHB = GetHashOfFile("/home/uttam/Desktop/abhi/new/hb/B.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/hb/WB.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/hb/C.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/hb/WC.txt")
                  +GetHashOfFile("/home/uttam/Desktop/abhi/new/hb/PKB.txt");
          writeStringToFile( preHB,"/home/uttam/Desktop/abhi/new/hb/preHB.txt");
          HB = GetHashOfFile("/home/uttam/Desktop/abhi/new/hb/preHB.txt");
          writeStringToFile(HB, "/home/uttam/Desktop/abhi/new/hb/HB.txt");
          
        
    }
    public void generateHbd() throws IOException, NoSuchAlgorithmException{
        //        b,wb,c,wc,pkb
          writeToFile("/home/uttam/Desktop/abhi/new/hbd/PKB.txt", Pkb.getEncoded());
          writeToFile("/home/uttam/Desktop/abhi/new/hbd/C.txt", C.getEncoded());
          writeStringToFile(B.toString(10), "/home/uttam/Desktop/abhi/new/hbd/B.txt");
          writeStringToFile(Wb.toString(10), "/home/uttam/Desktop/abhi/new/hbd/WB.txt");
          writeStringToFile(Wc.toString(10), "/home/uttam/Desktop/abhi/new/hbd/WC.txt");
          
          String preHBD = GetHashOfFile("/home/uttam/Desktop/abhi/new/hbd/B.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/hbd/WB.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/hbd/C.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/hbd/WC.txt")+GetHashOfFile("/home/uttam/Desktop/abhi/new/hbd/PKB.txt");
          writeStringToFile( preHBD,"/home/uttam/Desktop/abhi/new/hbd/preHBD.txt");
          HBD = GetHashOfFile("/home/uttam/Desktop/abhi/new/hbd/preHBD.txt");
          writeStringToFile(HBD, "/home/uttam/Desktop/abhi/new/hbd/HBD.txt");
          
        
    }
    
    
    public void generateSA(String data) throws FileNotFoundException, IOException{
        BufferedReader br=new BufferedReader(new FileReader(new File("/home/uttam/Desktop/abhi/new/ha/HA.txt")));
        
//        System.out.println(data);
        BigInteger hash = new BigInteger(br.readLine(), 16);
        BigInteger sign = hash.modPow(DA, NA);
        BigInteger verifySign = sign.modPow(A,NA);
        System.out.println("sign:"+sign.toString(16));
        System.out.println("verifySign:"+verifySign.toString(16));
//        writeStringToFile(sign.toString(16), "/home/uttam/Desktop/abhi/new/ha/SA.txt");
        List<byte[]> list;
        list = new ArrayList<byte[]>();
        list.add(data.getBytes());
        list.add(sign.toString(16).getBytes());

        File f = new File("/home/uttam/Desktop/abhi/new/ha/SA.txt");
        f.getParentFile().mkdirs();
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("/home/uttam/Desktop/abhi/new/ha/SA.txt"));
        out.writeObject(list);
        out.close();
//        System.out.println("Your file is ready.");
        
    }
    public void generateSB(String data) throws FileNotFoundException, IOException {
        BufferedReader br=new BufferedReader(new FileReader(new File("/home/uttam/Desktop/abhi/new/hb/HB.txt")));
        BigInteger hash = new BigInteger(br.readLine(), 16);
        BigInteger sign = hash.modPow(DB, NB);
        List<byte[]> list;
        list = new ArrayList<byte[]>();
        list.add(data.getBytes());
        list.add(sign.toString(16).getBytes());

        File f = new File("/home/uttam/Desktop/abhi/new/hb/SB.txt");
        f.getParentFile().mkdirs();
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("/home/uttam/Desktop/abhi/new/hb/SB.txt"));
        out.writeObject(list);
        out.close();
//        writeStringToFile(sign.toString(16), "/home/uttam/Desktop/abhi/new/hb/SB.txt");
    }
    private boolean verifySignatureSA(byte[] data, byte[] signature) throws Exception {
//        Signature sig = Signature.getInstance("SHA1withRSA");
//        sig.initVerify(getPublicVM(keyFile));
//        sig.update(data);
          String ha = new String(data);
          String sa = new String(signature);
          System.out.println("ha:"+ha+"\nsa:"+sa);
         
          BigInteger Bsa = new BigInteger(sa, 16);
//          BigInteger Bha = new BigInteger(ha, 16);
          BigInteger Bhad = Bsa.modPow(A,NA);
          System.out.println("Bhad:"+Bhad.toString(16)+"endofString");
          System.out.println("ha:"+ha+"endofString");
//          return Bha==Bhad;
//            return false;
          String SBhad = Bhad.toString(16);
          SBhad+="\n";
            return SBhad.equals(ha);
          
//        return sig.verify(signature);
    }

    private boolean verifySignatureSB(byte[] data, byte[] signature) throws Exception {
//        Signature sig = Signature.getInstance("SHA1withRSA");
//        sig.initVerify(getPublicVM(keyFile));
//        sig.update(data);
          String hb = new String(data);
          String sb = new String(signature);
//          System.out.println("ha:"+ha+"\nsa:"+sa);
         
          BigInteger Bsb = new BigInteger(sb, 16);
//          BigInteger Bha = new BigInteger(ha, 16);
          BigInteger Bhbd = Bsb.modPow(B,NB);
//          System.out.println("Bhad:"+Bhad.toString(16)+"endofString");
//          System.out.println("ha:"+ha+"endofString");
//          return Bha==Bhad;
//            return false;
          String SBhbd = Bhbd.toString(16);
          SBhbd+="\n";
            return SBhbd.equals(hb);
          
//        return sig.verify(signature);
    }
    public void verifySA(String filename) throws IOException, ClassNotFoundException, Exception{
        List<byte[]> list;
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
        list = (List<byte[]>) in.readObject();
        in.close();

        System.out.println(verifySignatureSA(list.get(0), list.get(1)) ? "VERIFIED MESSAGE" + "\n----------------\n" + new String(list.get(0)) : "Could not verify the signature.");     
    }
    public void verifySB(String filename) throws IOException, Exception{
        List<byte[]> list;
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
        list = (List<byte[]>) in.readObject();
        in.close();

        System.out.println(verifySignatureSB(list.get(0), list.get(1)) ? "VERIFIED MESSAGE" + "\n----------------\n" + new String(list.get(0)) : "Could not verify the signature.");     
        
    }
    public void generateSkca(){
//        Wa.modPow(, A)
//Wc=g.modPow(Rc,p);
           BigInteger p = new BigInteger("997");
          Skca= Wa.modPow(Rc, p);
    }
    public void generateSkcb(){
        BigInteger p = new BigInteger("997");
        Skca= Wb.modPow(Rc, p);
    }
    public void generateSkac(){
        if(omegaA.equals(omegaB)){
            BigInteger p = new BigInteger("997");
            Skca= Wc.modPow(Ra, p);
            System.out.println("Skca:"+Skca);
           
        }
        else{
            System.out.println("OMEGA A AND OMEGA B ARE NOT EQUAL");
            System.exit(0);
        }
    }
    public void generateSkbc(){
        if(omegaB.equals(omegaA)){
            BigInteger p = new BigInteger("997");
            Skcb= Wc.modPow(Rb, p);
            System.out.println("Skcb:"+Skcb);
        }
        else{
            System.out.println("OMEGA A AND OMEGA B ARE NOT EQUAL");
            System.exit(0);
        }
    }
    private int getInverse(String filename) throws IOException{
        String shash = readFileToString(filename);
        
           shash = shash.trim();
//           System.out.println("shash:"+shash+"end");
        BigInteger b = new BigInteger(shash, 16);
        BigInteger ten = new BigInteger("10", 10);
        BigInteger zero = new BigInteger("0", 10);
        Long res=0L;
        while(!b.equals(zero)){
                res+=b.remainder(ten).longValue();
                b = b.divide(ten);
            }
        b = BigInteger.valueOf(res);
        while(b.longValue() > 100L){
            res=0L;
            while(!b.equals(zero)){
                res+=b.remainder(ten).longValue();
                b = b.divide(ten);
            }
            b = BigInteger.valueOf(res);
        }
        return b.intValue();
            
//        Long ihash = Long.parseLong(shash, 16);
//        
//        while(ihash>100){
//            res=0L;
//            while(ihash>0){
//                res+=ihash%10;
//                ihash/=10;
//            }
//            ihash = res;
//        }
//        if ( ihash > (long)Integer.MAX_VALUE ) {
//            System.out.println("ihash is longer than int can represent".toUpperCase());
//            System.exit(0);
//        }
//        
//        return ihash.intValue();
//          return 1;
    }
    public byte[] encrypt(PublicKey publicKey,String message) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        System.out.println(message);
        return cipher.doFinal(message.getBytes());
        
    }
    public void generateEa() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException{
//        int powv = password1/getInverse("/home/uttam/Desktop/abhi/new/h1/H1.txt");
        int powv = password1;
        BigInteger bigG = BigInteger.valueOf(g);
        BigInteger Gpw1h1 = bigG.pow(powv);
        bEA = encrypt(Pka,Gpw1h1.toString(16));
        System.out.println("Encryption Gpw1h1:"+Gpw1h1.toString(16)+"end");
        System.out.println("\nbEA:"+bEA);
        
               
    }
    public void generateEb() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException{
//        int powv = password2/getInverse("/home/uttam/Desktop/abhi/new/h2/H2.txt");
        int powv = password2;
        BigInteger bigG = BigInteger.valueOf(g);
        BigInteger Gpw2h2 = bigG.pow(powv);
        bEB = encrypt(Pkb,Gpw2h2.toString(16));
        
    }
    private byte[] decrypt(PrivateKey privatekey,byte[] message ) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privatekey);
        
        return cipher.doFinal(message);
        
    }
    public void generateWa() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException{
        bEDA = decrypt(Ska,bEA);
        BigInteger bigpw1h1 = new BigInteger(new String(bEDA), 16);
        
//        BigInteger temp = bigpw1h1.pow(getInverse("/home/uttam/Desktop/abhi/new/h1d/H1d.txt"));
        BigInteger bigg = BigInteger.valueOf(g);
        BigInteger tmp = bigg.pow(passwordA);
        omegaA = bigpw1h1.divide(tmp);
        System.out.println("omegaA"+omegaA);
//        System.out.println("DE - Encryption Gpw1h1:"+new String(bEDA)+"end");
        
    }
    public void generateWb() throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
        bEDB = decrypt(Skb, bEB);
        BigInteger bigpw2h2 = new BigInteger(new String(bEDB), 16);
//        BigInteger temp = bigpw2h2.pow(getInverse("/home/uttam/Desktop/abhi/new/h2d/H2d.txt"));
        BigInteger bigg = BigInteger.valueOf(g);
        BigInteger tmp = bigg.pow(passwordB);
        
        omegaB = tmp.divide(bigpw2h2);
        System.out.println("omegaB"+omegaB);
        
    }
    
    public static void main(String[] args) throws NoSuchAlgorithmException, IOException, Exception {
        // TODO code application logic here
        Version2 v2 = new Version2();
	v2.getPassword();
        v2.generateAandDA(passwordA, g);
        v2.generateBandDB(passwordB,g);
        System.out.println(A);
        System.out.println(DA);
        System.out.println(B);
        System.out.println(DB);
        
        v2.generateCAndDc(4096);
        System.out.println(C);
        System.out.println(Dc);
        v2.generatePkaAndSka(4096);
        v2.generatePkbAndSkb(4096);
        v2.generateRaAndWa();
        v2.generateRbAndWb();
        v2.generateRcAndWc();
        System.out.println("Ra:"+Ra);
        System.out.println("Rb:"+Rb);
        System.out.println("Rc:"+Rc);
        System.out.println("Wa:"+Wa);
        System.out.println("Wb:"+Wb);
        System.out.println("Wc:"+Wc);
        
        v2.generateHa();
        v2.generateHb();
        
        v2.writeToFile("/home/uttam/Desktop/abhi/new/C.txt", C.getEncoded());
        v2.writeToFile("/home/uttam/Desktop/abhi/new/DC.txt", Dc.getEncoded());
        v2.writeToFile("/home/uttam/Desktop/abhi/new/PKA.txt", Pka.getEncoded());
        v2.writeToFile("/home/uttam/Desktop/abhi/new/SKA.txt", Ska.getEncoded());
        v2.writeToFile("/home/uttam/Desktop/abhi/new/PKB.txt", Pkb.getEncoded());
        v2.writeToFile("/home/uttam/Desktop/abhi/new/SKB.txt", Skb.getEncoded());
        v2.generateSA(v2.readFileToString("/home/uttam/Desktop/abhi/new/ha/HA.txt"));
//        BigInteger trash = new BigInteger("ffda0275bfe6a115dc9ed981f964a7b76825d1f3", 16);
//        System.out.println(trash);
        v2.generateSB(v2.readFileToString("/home/uttam/Desktop/abhi/new/hb/HB.txt"));
        v2.generateHad();
        v2.generateHbd();
        v2.verifySA("/home/uttam/Desktop/abhi/new/ha/SA.txt");
        v2.verifySB("/home/uttam/Desktop/abhi/new/hb/SB.txt");
        v2.generateSkca();
        v2.generateSkcb();
        v2.generatePassword1AndPassword2();
        v2.generateH1();
        v2.generateH2();
        v2.generateEa();
        v2.generateEb();
        v2.generateH1d();
        v2.generateH2d();
        v2.generateWa();
        v2.generateWb();
        v2.generateSkac();
        v2.generateSkbc();
//        v2.generateSA("/home/uttam/Desktop/abhi/new/ha/HA", "/home/uttam/Desktop/abhi/new/DA", "/home/uttam/Desktop/abhi/new/ha/SA");
//        v2.generateSA("/home/uttam/Desktop/abhi/new/hb/HB", "/home/uttam/Desktop/abhi/new/DB", "/home/uttam/Desktop/abhi/new/hb/SB");
//        System.out.println(v2.readFileToString("/home/uttam/Desktop/abhi/new/ha/HA"));
    }   
    
}
